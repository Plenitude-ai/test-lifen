import json
import logging
import pathlib

import fire

logging_fmt = "[%(asctime)s - %(funcName)s - %(levelname)s] : %(message)s"
logging.basicConfig(format=logging_fmt, level=logging.DEBUG)

CUR_DIR = pathlib.Path(__file__).parent


def extract_names(document: dict[str, list[dict[str, list[dict]]]]) -> list[str]:
    """
    Extracts names for each page, in a list of strings
    """
    names = []
    for page in document["pages"]:
        words_infos = sorted(
            page["words"], key=lambda info: info["bbox"]["y_min"] * 100 + info["bbox"]["x_min"]
        )
        logging.info(" ".join([word_info["text"] for word_info in words_infos]))
        for i, word_info in enumerate(words_infos[:-1]):
            word: str = word_info["text"]
            word_position: dict[str, float] = word_info["bbox"]

            next_word_info = words_infos[i + 1]
            next_word: str = next_word_info["text"]
            next_word_position: dict[str, float] = next_word_info["bbox"]

            if (word.lower() in ["monsieur", "madame", "m.", "mr", "mme", "patiente"]) and (
                next_word not in names
            ):
                if abs(next_word_position["y_min"] - word_position["y_min"]) < 0.02:
                    names.append(next_word)
            if (word in names) and (not next_word[0].islower()) and (next_word not in names):
                names.append(next_word)
    return names


def main(document_path: str) -> list[str]:
    """
    Extracts name as a single string

    - document_path: str, path to json data

    Returns:
        - list[str] : list of words identified as name
    """
    with open(str(CUR_DIR / document_path)) as f:
        document = json.load(f)
    return extract_names(document)


if __name__ == "__main__":
    fire.Fire(main)
