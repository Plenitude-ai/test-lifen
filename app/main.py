import logging

from fastapi import FastAPI, HTTPException

from app import models
from app import heuristic

app = FastAPI()

logging_fmt = "[%(asctime)s - %(funcName)s - %(levelname)s] : %(message)s"
logging.basicConfig(format=logging_fmt, level=logging.DEBUG)


@app.post("/extract_names")
async def extract_names(document: models.Document):
    try:
        logging.info("Called endpoint...")
        names = heuristic.extract_names(document.model_dump())
        logging.info("Returning names : %s", names)
        return {"names": names}
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
