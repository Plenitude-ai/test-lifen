"""Pydantic models for API swagger documentation"""

import pydantic


class BBox(pydantic.BaseModel):
    x_min: float
    x_max: float
    y_min: float
    y_max: float

    model_config = {
        "json_schema_extra": {
            "examples": [{"x_min": 0.36, "x_max": 0.44, "y_min": 0.09, "y_max": 0.1}]
        }
    }


class Word(pydantic.BaseModel):
    text: str = pydantic.Field(json_schema_extra={"examples": ["identified_word"]})
    bbox: BBox


class Page(pydantic.BaseModel):
    words: list[Word]


class Document(pydantic.BaseModel):
    pages: list[Page]
    original_page_count: float = pydantic.Field(
        json_schema_extra={
            "examples": [1],
            "description": "must match the number of pages provided in the `page` field",
        }
    )
    needs_ocr_case: str = pydantic.Field(json_schema_extra={"examples": ["no_ocr"]})
