install:
	(cd $(pwd) & poetry install)

help:
	poetry run python app/heuristic.py --help

test:
	poetry run pytest .

process:
	poetry run python app/heuristic.py data.json

run:
	uvicorn main:app --app-dir app --host 0.0.0.0 --port 3400 --reload

deploy:
	docker-compose up --build

push:
	docker-compose push
