# Lifen technical test

This is a text detection problem. We could also imagine that it is just a text identification problem as we can certainly argue that in 99% of the cases, the target is present, and is a unique one. But the edge case where there is several patients (maybe siblings prenting hte same health issue for example), or the one where the name is missed and not written at all, probably make it a detection problem.  
For the sake of simplicity, we'll just assume this is an identification problem. We'll just try to find in the document the word which is the most likely to belong to each target `Name` and `Last Name`.

## 1. Simple Heuristic

We're going to identify the nearest word to each `Madame`/`Monsieur`, `Patient(e)`, or `Mme`/`M.` (often misspelled as `Mr`). If the next word is capitalized at its first letter, then it is considered as the last name.


## 2. ML approach

The most straightforward method would be to create our own NER (Name Entity Recognition).
It would be run on 
We can use pretrained embeddings (openAI, or fastText or Word2Vec) to get a vector for each of the words of our corpus.
Then we can add some context features, like the word position, if it was capitalized, and the embedding of the sentence it belongs to.

We can also add the absolute position in the page, but it seems from the examples given, that it will not be a very reliable source of information. Sometimes the sender is written on top of the page, it is the receiver which is written.
But we can just try and see if the results are improving.

This idea should give a high score, and has the advantage to also exhibit the reliability of the 

`ML pipeline fully in-house, a solution that should achieve high accuracy, fast and cost-efficient during inference (millions of real-time predictions per week).`

I thought of 2 different ideas.
- Finetuning a NER model.
- Classical ML classifier based on :
    - Small word embedding (fastText or Word2Vec for instance)
    - Embedding of the entire sentence it belongs to (for context)
    - Capitalization (first letter, all, none...)
    - Position in the page   
It could be a few neural network layers, or a SVM/XGBoost... Hyperparameter job could give us a hint

### 2.1 NER
The Ner approach :
- is straightforward.
- should be quite fast, if based on a distill-Bert for example
- But not really adaptable, need to retrain all custom layers when adding a new feature

`What do you need to build this solution?`
- Build coherent text dataset.
    - Sorting of words based on position.   
    (Maybe use a KNN/DBSCAN algorithm to find cluster, and nly then reorder the words.)
- Define the categories we need : `first name` and `last names` to begin with.

`What's your process for building this algorithm?`
- Create dataset by tagging each occurence of these `first` and `last name`
- Fine-tune a Camembert-based NER on this dataset
- During inference, add a small heuristic layer, that uses NER output and selects the word with most confidence for each category.
- Evaluate performance on test set.
- Decide to go on production or not

### 2.2 Classifiers
The classical ML approach:
- is probably easier to retrain for new features
- should be faster to compute
- takes into account more variety of data (positional, capitalization etc.)
- maybe not as performant as a dedicated NER.

`What do you need to build this solution?`
- The dataset also needs to be processed (extract readable text from JSON) and each word should be assigned its categories: first name, last name, maybe adress, drug name etc
- Embeddings, classifiers, hyperparameter solver (optuna for example)

`What's your process for building this algorithm?`
- build training dataset
- create a sklearn Pipeline with all the steps for processing the data
- train and select best hyperparameters with kfold and bayesian optimizer (optuna) -> this will help us select the best model, the best config in general
- verify that the performance reached is enough with the PM before pushing to production

### 2.3 General
`What kind of algorithms do you want to try? (You may talk about different approaches and talk about pros & cons)`
- Ner approach
- Classical statistical approach


`What features would you engineer (if any)?`
- If available font style (we can get it with pdf documents, if it is indeed the source of the data)
- capitalization
- length of the word itself
- size of the paragraph it belongs to 

`Think of edge cases (complicated or ambiguous docs: we are deployed in more than 700 healthcare organisations and people sometimes get very creative) and how you would handle them.`

## 3. ML solution using an external API (LLMs).
`How would you leverage this kind of model to solve this problem?`   
We can try to find the best prompt, or event use asynchronous calls and create a prompt for each category.

`What is the advantage and disadvantage of this approach compared to the previous one?`   
Pros :
- Very easy to maintain, and evolve over time -> if there is a new category, a few hours to find the best prompt, evaluate its perf on a test set, and we can push the new prompt to production, no code required
- should perform very well

Cons :
- Dependent to the availabaility of the model
- could be costly, and budget can drastically change without warning
- does not support Batch processing (even though we're talking of real time on this specific subject, we could want to use a batch processing at some point)
- need to be careful about the disclosure of personal data (carefully read terms of use, and their evolution)
- need a bit of heuristic to parse the result, making sure it is of correct format (there is some features to configure a specific output type, json for example)

## 4. Option -> fine tune our own LLM

Fine tuning our own LLM more or less gives us all the advantages of the LLM APIs, but without most of their drawbacks
- always available
- can still be costly, but it is more predictable
- Supports batch processing if needed
- No data disclosure issue
- fine tuned, should be very reliable on the output type (JSON for example)

But it obviously needs a lot more of development efforts. It is however a strong asset for future ideas.


