import json
import pathlib
import sys

sys.path.append("../app")

from fastapi.testclient import TestClient

from app.main import app

CUR_DIR = pathlib.Path(__file__).parent

client = TestClient(app)
with open(str(CUR_DIR / "../app/data.json")) as f:
    input_dict = json.load(f)


def test_extract_names():
    response = client.post("/extract_names", json=input_dict)

    assert response.status_code == 200
    assert response.json() == {"names": ["Jean", "DUPONT"]}
